package ru.t1.shevyreva.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.shevyreva.tm.model.User;

@Setter
@Getter
@NoArgsConstructor
public class UserUpdateProfileResponse extends AbstractUserResponse {

    public UserUpdateProfileResponse(User user) {
        super(user);
    }

}
