package ru.t1.shevyreva.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class TaskRemoveByProjectIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    public TaskRemoveByProjectIdRequest(@Nullable final String projectId) {
        this.projectId = projectId;
    }

}
