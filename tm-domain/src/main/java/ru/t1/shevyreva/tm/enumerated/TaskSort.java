package ru.t1.shevyreva.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.comporator.DateComparator;
import ru.t1.shevyreva.tm.comporator.NameComparator;
import ru.t1.shevyreva.tm.comporator.StatusComparator;
import ru.t1.shevyreva.tm.model.Task;

import java.util.Comparator;

public enum TaskSort {

    BY_NAME("Sort by name.", NameComparator.INSTANCE::compare),
    BY_STATUS("Sort by status.", StatusComparator.INSTANCE::compare),
    BY_CREATED("Sort by created.", DateComparator.INSTANCE::compare);

    @Nullable
    private final String displayName;

    @Nullable
    private final Comparator<Task> comparator;

    TaskSort(@Nullable final String displayName, @Nullable final Comparator<Task> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @Nullable
    public static TaskSort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@NotNull final TaskSort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

    @Nullable
    public String getDisplayName() {
        return displayName;
    }

    @Nullable
    public Comparator<Task> getComparator() {
        return comparator;
    }

}
