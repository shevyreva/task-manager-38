package ru.t1.shevyreva.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.api.DBConstants;
import ru.t1.shevyreva.tm.api.repository.IProjectRepository;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.model.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Override
    protected String getTableName() {
        return DBConstants.TABLE_PROJECT;
    }

    @Override
    public @NotNull Project fetch(@NotNull ResultSet row) throws Exception {
        @NotNull final Project project = new Project();
        project.setId(row.getString(DBConstants.COLUMN_ID));
        project.setUserId(row.getString(DBConstants.COLUMN_USER_ID));
        project.setName(row.getString(DBConstants.COLUMN_NAME));
        project.setDescription(row.getString(DBConstants.COLUMN_DESCRIPTION));
        project.setStatus(Status.toStatus(row.getString(DBConstants.COLUMN_STATUS)));
        return project;
    }

    @Override
    @SneakyThrows
    public @NotNull Project add(@NotNull Project project) {
        @NotNull final String sql = String.format("INSERT INTO %s (%s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?)",
                getTableName(), DBConstants.COLUMN_ID, DBConstants.COLUMN_USER_ID, DBConstants.COLUMN_NAME,
                DBConstants.COLUMN_DESCRIPTION, DBConstants.COLUMN_STATUS, DBConstants.COLUMN_CREATED);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getId());
            statement.setString(2, project.getUserId());
            statement.setString(3, project.getName());
            statement.setString(4, project.getDescription());
            statement.setString(5, project.getStatus().toString());
            statement.setTimestamp(6, new Timestamp(project.getCreated().getTime()));
            statement.executeUpdate();
        }
        return project;
    }

    @Override
    public @NotNull Project add(@NotNull String userId, @NotNull Project project) {
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @SneakyThrows
    public Project update(Project project) {
        @NotNull final String sql = String.format("UPDATE %s SET %s = ?, %s = ?, %s = ? WHERE %s = ?",
                getTableName(), DBConstants.COLUMN_NAME,
                DBConstants.COLUMN_DESCRIPTION, DBConstants.COLUMN_STATUS, DBConstants.COLUMN_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getName());
            statement.setString(2, project.getDescription());
            statement.setString(3, project.getStatus().toString());
            statement.setString(4, project.getId());
            statement.executeUpdate();
        }
        return project;
    }

}
