package ru.t1.shevyreva.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.repository.ITaskRepository;
import ru.t1.shevyreva.tm.api.service.IConnectionService;
import ru.t1.shevyreva.tm.api.service.ITaskService;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.enumerated.TaskSort;
import ru.t1.shevyreva.tm.exception.entity.StatusNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.TaskNotFoundException;
import ru.t1.shevyreva.tm.exception.field.*;
import ru.t1.shevyreva.tm.model.Task;
import ru.t1.shevyreva.tm.repository.TaskRepository;

import java.sql.Connection;
import java.util.List;

public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    protected Connection getConnection() {
        return connectionService.getConnection();
    }

    @NotNull
    @Override
    protected ITaskRepository getRepository(@NotNull Connection connection) {
        return new TaskRepository(connection);
    }

    @NotNull
    @SneakyThrows
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        @NotNull Connection connection = getConnection();
        try {
            @NotNull ITaskRepository repository = getRepository(connection);
            repository.add(userId, task);
            connection.commit();
            return task;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    public Task create(@Nullable final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final Task task = new Task();
        task.setName(name);
        @NotNull Connection connection = getConnection();
        try {
            @NotNull ITaskRepository repository = getRepository(connection);
            repository.add(userId, task);
            connection.commit();
            return task;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    public Task updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description) {
        if (index == null || index < 0) throw new IndexEmptyException();
        if (getSize() < index) throw new IndexEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        @NotNull Connection connection = getConnection();
        try {
            @NotNull ITaskRepository repository = getRepository(connection);
            repository.update(task);
            connection.commit();
            return task;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    public Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        @NotNull Connection connection = getConnection();
        try {
            @NotNull ITaskRepository repository = getRepository(connection);
            repository.update(task);
            connection.commit();
            return task;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    public Task changeTaskStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status) {
        if (index == null || index < 0) throw new IndexEmptyException();
        if (getSize() < index) throw new IndexEmptyException();
        if (status == null) throw new StatusNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        @NotNull Connection connection = getConnection();
        try {
            @NotNull ITaskRepository repository = getRepository(connection);
            repository.update(task);
            connection.commit();
            return task;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    public Task changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        @NotNull Connection connection = getConnection();
        try {
            @NotNull ITaskRepository repository = getRepository(connection);
            repository.update(task);
            connection.commit();
            return task;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull Connection connection = getConnection()) {
            @NotNull ITaskRepository repository = getRepository(connection);
            return repository.findAllByProjectId(userId, projectId);
        }
    }

    @NotNull
    @SneakyThrows
    public List<Task> findAll(@Nullable final String userId, @Nullable final TaskSort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        try (@NotNull Connection connection = getConnection()) {
            @NotNull ITaskRepository repository = getRepository(connection);
            return repository.findAll(userId, sort.getComparator());
        }
    }

}
