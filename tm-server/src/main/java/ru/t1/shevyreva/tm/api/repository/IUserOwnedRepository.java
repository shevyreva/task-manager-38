package ru.t1.shevyreva.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    @NotNull
    M add(String userId, M model) throws Exception;

    @NotNull
    List<M> findAll(String userId) throws Exception;

    @NotNull
    List<M> findAll(String userId, Comparator<M> comparator) throws Exception;

    void removeAll(String userId) throws Exception;

    @Nullable
    M findOneById(String userId, String id) throws Exception;

    @Nullable
    M findOneByIndex(String userId, Integer index) throws Exception;

    @NotNull
    M removeOne(String userId, M model) throws Exception;

    @NotNull
    M removeOneById(String userId, String id) throws Exception;

    @Nullable
    M removeOneByIndex(String userId, Integer index) throws Exception;

    boolean existsById(String userId, String id) throws Exception;

    @NotNull
    int getSize(String userId) throws Exception;

}
