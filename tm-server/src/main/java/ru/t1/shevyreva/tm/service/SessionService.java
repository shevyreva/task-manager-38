package ru.t1.shevyreva.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.api.repository.ISessionRepository;
import ru.t1.shevyreva.tm.api.service.IConnectionService;
import ru.t1.shevyreva.tm.api.service.ISessionService;
import ru.t1.shevyreva.tm.model.Session;
import ru.t1.shevyreva.tm.repository.SessionRepository;

import java.sql.Connection;

public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    protected ISessionRepository getRepository(@NotNull Connection connection) {
        return new SessionRepository(connection);
    }

}
