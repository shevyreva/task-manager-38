package ru.t1.shevyreva.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.repository.IUserRepository;
import ru.t1.shevyreva.tm.api.service.IConnectionService;
import ru.t1.shevyreva.tm.api.service.IPropertyService;
import ru.t1.shevyreva.tm.api.service.IUserService;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.exception.field.EmailEmptyException;
import ru.t1.shevyreva.tm.exception.field.IdEmptyException;
import ru.t1.shevyreva.tm.exception.field.LoginEmptyException;
import ru.t1.shevyreva.tm.exception.field.PasswordEmptyException;
import ru.t1.shevyreva.tm.exception.user.ExistsEmailException;
import ru.t1.shevyreva.tm.exception.user.ExistsLoginException;
import ru.t1.shevyreva.tm.exception.user.RoleEmptyException;
import ru.t1.shevyreva.tm.model.User;
import ru.t1.shevyreva.tm.repository.UserRepository;
import ru.t1.shevyreva.tm.util.HashUtil;

import java.sql.Connection;

public class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    IPropertyService propertyService;

    public UserService(@NotNull final IConnectionService connectionService,
                       @NotNull final IPropertyService propertyService
    ) {
        super(connectionService);
        this.propertyService = propertyService;
    }

    protected Connection getConnection() {
        return connectionService.getConnection();
    }

    @NotNull
    @Override
    protected IUserRepository getRepository(@NotNull Connection connection) {
        return new UserRepository(connection);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();

        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password, propertyService));
        user.setRole(Role.USUAL);

        @NotNull Connection connection = getConnection();
        try {
            @NotNull IUserRepository repository = getRepository(connection);
            repository.add(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new ExistsEmailException();
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();

        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password, propertyService));
        user.setRole(Role.USUAL);
        user.setEmail(email);

        @NotNull Connection connection = getConnection();
        try {
            @NotNull IUserRepository repository = getRepository(connection);
            repository.add(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    @SneakyThrows
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();

        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password, propertyService));
        user.setRole(role);

        @NotNull Connection connection = getConnection();
        try {
            @NotNull IUserRepository repository = getRepository(connection);
            repository.add(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        try (@NotNull Connection connection = getConnection()) {
            @NotNull IUserRepository repository = getRepository(connection);
            return repository.findByLogin(login);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        try (@NotNull Connection connection = getConnection()) {
            @NotNull IUserRepository repository = getRepository(connection);
            return repository.findByEmail(email);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull Connection connection = getConnection();
        try {
            @NotNull IUserRepository repository = getRepository(connection);
            @NotNull final User user = repository.findByLogin(login);
            repository.removeOne(user);
            connection.commit();
            return user;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull Connection connection = getConnection();
        try {
            @NotNull IUserRepository repository = getRepository(connection);
            @NotNull final User user = repository.findByEmail(email);
            repository.removeOne(user);
            connection.commit();
            return user;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String middleName,
            @Nullable final String lastName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull Connection connection = getConnection();
        try {
            @NotNull IUserRepository repository = getRepository(connection);
            @NotNull final User user = findOneById(id);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setMiddleName(middleName);
            repository.update(user);
            connection.commit();
            return user;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull Connection connection = getConnection();
        try {
            @NotNull IUserRepository repository = getRepository(connection);
            @NotNull final User user = findOneById(id);
            user.setPasswordHash(HashUtil.salt(password, propertyService));
            repository.update(user);
            connection.commit();
            return user;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull Connection connection = getConnection();
        try {
            @NotNull IUserRepository repository = getRepository(connection);
            return repository.isLoginExist(login);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull Connection connection = getConnection();
        try {
            @NotNull IUserRepository repository = getRepository(connection);
            return repository.isLoginExist(email);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    public User lockUser(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull Connection connection = getConnection();
        try {
            @NotNull IUserRepository repository = getRepository(connection);
            @NotNull User user = findByLogin(login);
            user.setLocked(true);
            repository.update(user);
            connection.commit();
            return user;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }

    }

    @NotNull
    @SneakyThrows
    public User unlockUser(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull Connection connection = getConnection();
        try {
            @NotNull IUserRepository repository = getRepository(connection);
            @NotNull User user = findByLogin(login);
            user.setLocked(false);
            repository.update(user);
            connection.commit();
            return user;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
