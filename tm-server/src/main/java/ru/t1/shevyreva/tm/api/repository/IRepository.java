package ru.t1.shevyreva.tm.api.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    M add(@NotNull final M model) throws Exception;

    @NotNull
    @SneakyThrows
    Collection<M> add(@NotNull Collection<M> models);

    @SneakyThrows
    Collection<M> set(@NotNull final Collection<M> models);

    void clear() throws Exception;

    @NotNull
    @SneakyThrows
    List<M> findAll() throws Exception;

    @NotNull
    @SneakyThrows
    List<M> findAll(@NotNull Comparator<M> comparator) throws Exception;

    @Nullable
    @SneakyThrows
    M findOneById(@NotNull String Id);

    @NotNull
    @SneakyThrows
    M findOneByIndex(@NotNull Integer index);

    @NotNull
    @SneakyThrows
    M removeOne(@NotNull M model);

    @NotNull
    @SneakyThrows
    M removeOneById(@NotNull String Id) throws Exception;

    @SneakyThrows
    void removeOneByIndex(@NotNull Integer index) throws Exception;

    @SneakyThrows
    boolean existsById(@NotNull String id) throws Exception;

    @NotNull
    @SneakyThrows
    int getSize() throws Exception;

}
