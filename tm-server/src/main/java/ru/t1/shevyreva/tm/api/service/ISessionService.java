package ru.t1.shevyreva.tm.api.service;

import ru.t1.shevyreva.tm.api.repository.IUserOwnedRepository;
import ru.t1.shevyreva.tm.model.Session;

public interface ISessionService extends IUserOwnedRepository<Session> {
}
