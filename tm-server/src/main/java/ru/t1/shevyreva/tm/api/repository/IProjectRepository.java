package ru.t1.shevyreva.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {
/*
    @NotNull
    Project create(@NotNull String userId, @NotNull String name, @NotNull String description) throws Exception;

    @NotNull
    Project create(@NotNull String userId, @NotNull String name) throws Exception;

    @NotNull
    List<Project> findAll(@NotNull String userId, @NotNull ProjectSort sort) throws Exception;
*/

    public @NotNull Project add(@NotNull Project project) throws Exception;

    public @NotNull Project add(@NotNull String userId, @NotNull Project project) throws Exception;

    public Project update(Project project) throws Exception;
}
